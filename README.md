# WalletDemo


## Overview 🤝

This is a demo app showing basic functionality of the `WalletSDK`. 

### Configuration 🛠️

In order to run this you will need to populate all the values in the `Production.xcconfig` file.

If you want to learn more about Xcode config files you can do so [here](https://nshipster.com/xcconfig/) 

You also need a valid user ID. This demo app does not have a way for you to make one. You'll need to use a `cURL` command. Once you create one, you'll need to hard code this value in `RootView`. 

### Provisioning & Entitlements 📜

In order for the application to build and run, Xcode will validate that your bundle ID and CloudKit container ID are correct given your developer account credentials. You can learn more about this [here](https://developer.apple.com/documentation/xcode/configuring-icloud-services), but you should not have to change anything in the project other than what's in the `.xcconfig` file. You will however need an Apple Developer account. You'll need to create a bundle identifier, provisioning profile for the given identifier, and you'll need to setup the iCloud capability for the given profile. You can look at your iCloud database [here](https://icloud.developer.apple.com/dashboard/database/). The database ID should be reflected in the provisioning profile. Enabled capabilities in the provisioning profile should be `Associated Domains` and `iCloud` at a minimum. When you add `iCloud` as a capability, you should be able to set the database ID in the developer portal. None of this is done in Xcode. 

Once all of this is complete, go to `Xcode` -> `Settings` -> `Accounts` and sign in and select download profiles (assuming you do not already have the profiles downloaded). If everything is setup correctly, you should be able to run the project in your iOS simulator. 

### Tips 💡

Please note that once you authenticate and create a wallet with this user ID, **do not create additional wallets** for this user ID. If you do, the SDK will not know what key IDs to use to craft transactions. If you want to use a single user ID repeatedly, you'll need to implement the `doesWalletExistForUserId` function. This will return `true` if a wallet exists for the given user ID. Then you won't need to create a wallet and you can continue to craft and send transactions. You just need to set the user ID, confirm a wallet already exists for it, authenticate succesfully, and then you can derive addresses and submit transactions.

If you want to run this in the simulator, after launching the simulator you'll need to go to `Features`, `Face ID` and then select `Enrolled`.
When you select `Authenticate`, the passkey authentication window should appear, assuming your `Production.xcconfig` is correctly populated. When the window appears you must select `Matching Face` in the same `Face ID` menu. This simulates authenticating.

Once you do this you can Create a Wallet. 

In order to send funds, you will need to derive your crypto addresses, and then fund those addresses with test funds. 

There are additional notes and tips in the `RootView` file.

### Links 🔗

 - [Official WalletSDK Documentation](https://docs.wallet-sdk.blockdaemon.com/docs/quickstart).
 - [Associated Domains on iOS](https://developer.apple.com/documentation/xcode/supporting-associated-domains)
 - [Entitlements on iOS](https://developer.apple.com/documentation/bundleresources/entitlements)
 - [Using Face ID in the Simulator](https://developer.apple.com/documentation/xcode/testing-complex-hardware-device-scenarios-in-simulator#Test-Face-ID-or-Touch-ID-authentication)
 - [Creating an iCloud Container](https://developer.apple.com/help/account/manage-identifiers/create-an-icloud-container/)
