// Copyright © Blockdaemon All rights reserved.

import SwiftUI
import WalletSDK

struct RootView: View {
    
    @State private var signedTransaction: String?
    @State private var encryptedBackup: EncryptedBackup?
    
    var body: some View {
        NavigationView {
            VStack {
                Text("Authentication")
                RoundedButton(title: "Authenticate") {
                    Task {
                        do {
                            // NOTE: You'll need to create your own user ID and set the user ID here.
                            WalletSDK.shared.set(userId: "")
                            try await WalletSDK.shared.auth.authenticate()
                        } catch {
                            print("Failed to authenticate: \(error)")
                        }
                    }
                }
                RoundedButton(title: "Sign Out") {
                    Task {
                        do {
                            await WalletSDK.shared.auth.signout()
                        }
                    }
                }
                Spacer()
                Text("Wallet Management")
                VStack {
                    HStack {
                        RoundedButton(title: "Create") {
                            Task {
                                do {
                                    // NOTE: This will create a new wallet every single time. If you have
                                    // already made one for the given user ID, it will make another. Rather than make
                                    // a new wallet, you should check to see if a wallet exists for the given user ID prior to creation.
                                    // If multiple wallets have been made for a given user ID, you will not be able to correctly create and send
                                    // transactions.
                                    // Make a new user ID each time you want to try creating a wallet. Then send test funds to the addresses
                                    // after derivation.
                                    let wallet = try await WalletSDK.shared.wallet.create()
                                    print(wallet)
                                } catch {
                                    print("Failed to create wallet: \(error)")
                                }
                            }
                        }

                        RoundedButton(title: "Restore") {
                            Task {
                                do {
                                    if let encryptedBackup = encryptedBackup {
                                        try await WalletSDK.shared.wallet.restore(
                                            encryptedBackup.backup,
                                            encryptionKey: encryptedBackup.encryptionKey
                                        )
                                        print("Restored wallet.")
                                    } else {
                                        print("You need an encrypted backup")
                                    }
                                } catch {
                                    print("Failed to restore wallet: \(error)")
                                }
                            }
                        }
                        .disabled(encryptedBackup == nil)
                    }
                    RoundedButton(title: "Export") {
                        Task {
                            do {
                                self.encryptedBackup = try await WalletSDK.shared.wallet.export()
                                print("Encrypted Backup: \(String(describing: encryptedBackup))")
                            } catch {
                                print("Failed to generate backup: \(error)")
                            }
                        }
                    }
                }
                Spacer()
                Text("Transactions")
                VStack {
                    HStack {
                        RoundedButton(title: "Preview") {
                            Task {
                                do {
                                    // NOTE: To generate a transaction your wallet likely needs a balance or this will fail.
                                    let tx: UnsignedTransaction<Intent> = try await self.createUnsignedTransactionWithInputs(
                                        EthereumInputs(
                                            // NOTE: Recommend testers use a destination address they have access to and can return funds from.
                                            destination: "0xcBB14CB8cE496EdD5FAD0736E1Aef05E149847C6",
                                            amount: "0.0001"
                                        ),
                                        supportedProtocol: .ethereumHolesky
                                    )
                                    signedTransaction = try await self.signUnsignedTransaction(
                                        tx,
                                        supportedProtocol: .ethereumHolesky
                                    )
                                } catch {
                                    print("Failed to generate unsigned tx: \(error)")
                                }
                            }
                        }
                        RoundedButton(title: "Derive Address") {
                            Task {
                                do {
                                    let eth = try await WalletSDK.shared.wallet.createCryptoAddressForBlockchain(
                                        "ethereum",
                                        network: "holesky",
                                        curveName: "secp256k1"
                                    )
                                    let sol = try await WalletSDK.shared.wallet.createCryptoAddressForBlockchain(
                                        "solana",
                                        network: "testnet",
                                        curveName: "ED-25519"
                                    )
                                    let btc = try await WalletSDK.shared.wallet.createCryptoAddressForBlockchain(
                                        "bitcoin",
                                        network: "testnet",
                                        curveName: "secp256k1"
                                    )
                                    print("BTC Address: \(btc)")
                                    print("SOL Address: \(sol)")
                                    print("ETH Address: \(eth)")
                                } catch {
                                    print("Failed to create address: \(error)")
                                }
                            }
                        }
                    }
                    RoundedButton(title: "Send") {
                        Task {
                            do {
                                if let tx = signedTransaction {
                                    let hash = try await self.submitSignedTransaction(tx, supportedProtocol: .ethereumHolesky)
                                    print(hash)
                                }
                            } catch {
                                print("Failed to submit signed tx: \(error)")
                            }
                        }
                    }
                    .disabled(signedTransaction == nil)
                }
            }
        }
        .padding(.all, 16.0)
        .navigationTitle("Wallet Demo")
    }
    
    private func createUnsignedTransactionWithInputs<
        Inputs: Encodable,
        Intent: Decodable
    >(
        _ inputs: Inputs,
        supportedProtocol: SupportedProtocol
    ) async throws -> UnsignedTransaction<Intent> {
        try await WalletSDK.shared.transaction.createUnsignedTransactionWithInputs(
            inputs,
            supportedProtocol: JSONEncoder().encode(supportedProtocol)
        )
    }

    private func signUnsignedTransaction<Intent: Decodable>(
        _ unsignedTransaction: UnsignedTransaction<Intent>,
        supportedProtocol: SupportedProtocol
    ) async throws -> String {
        try await WalletSDK.shared.transaction.signUnsignedTransaction(
            unsignedTransaction,
            supportedProtocol: JSONEncoder().encode(supportedProtocol)
        )
    }
    
    private func submitSignedTransaction(
        _ transaction: String,
        supportedProtocol: SupportedProtocol
    ) async throws -> String {
        try await WalletSDK.shared.transaction.submitSignedTransaction(transaction, protocolId: supportedProtocol.id)
    }
}

struct Intent: Decodable {
    let amount: String
    let destination: String
    let nonce: String
}

struct EthereumInputs: Encodable {
    let destination: String
    let amount: String
}

struct SupportedProtocol: Encodable {
    let blockchain: String
    let chainPath: [Int]
    let chainId: Int
    let curveName: String
    let keyType: String
    let id: String
}

extension SupportedProtocol {
    static let ethereumHolesky: SupportedProtocol = .init(
        blockchain: "ethereum",
        chainPath: [44,60,0,0,0],
        chainId: 17000,
        curveName: "secp256k1",
        keyType: "ecdsa",
        id: "ethereum-holesky"
    )
}

#Preview {
    RootView()
}
