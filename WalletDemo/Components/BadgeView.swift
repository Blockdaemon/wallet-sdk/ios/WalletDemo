// Copyright © Blockdaemon All rights reserved.

import SwiftUI

public struct BadgeView: View {

    public enum Style {
        case info
        case error
        case warning
        case success
    }

    public let title: String
    public let style: Style

    public init(title: String, style: Style) {
        self.title = title
        self.style = style
    }

    public var body: some View {
        ZStack {
            Text(title)
                .foregroundColor(style.textColor)
                .font(.body)
        }
        .padding(EdgeInsets(top: 6, leading: 8, bottom: 6, trailing: 8))
        .background(style.backgroundColor)
        .cornerRadius(8.0)
    }
}

extension BadgeView.Style {

    var backgroundColor: Color {
        let color: Color
        switch self {
        case .info:
            color = .gray.opacity(0.2)
        case .error:
            color = .red.opacity(0.2)
        case .warning:
            color = .orange.opacity(0.2)
        case .success:
            color = .green.opacity(0.2)
        }
        return color
    }

    var textColor: Color {
        let color: Color
        switch self {
        case .info:
            color = .gray
        case .error:
            color = .red
        case .warning:
            color = .orange
        case .success:
            color = .green
        }
        return color
    }
}
