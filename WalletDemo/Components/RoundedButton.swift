// Copyright © Blockdaemon All rights reserved.

import SwiftUI

struct RoundedButton<LeadingView: View>: View {
    let title: String
    let isLoading: Bool
    let leadingView: () -> LeadingView
    let action: () -> Void

    public init(
        title: String,
        isLoading: Bool = false,
        @ViewBuilder leadingView: @escaping () -> LeadingView,
        action: @escaping () -> Void
    ) {
        self.title = title
        self.isLoading = isLoading
        self.leadingView = leadingView
        self.action = action
    }
    
    var body: some View {
        Button(action: action) {
            HStack(spacing: 8.0) {
                leadingView()
                Text(title)
                    .font(.headline)
                    .foregroundColor(.white)
                if isLoading {
                    ProgressView()
                        .progressViewStyle(CircularProgressViewStyle(tint: .white))
                }
            }
            .padding(.horizontal)
            .frame(maxWidth: .infinity, minHeight: 45)
            .background(
                GeometryReader { geometry in
                    Color.purple
                        .cornerRadius(geometry.size.height / 2.0)
                }
            )
        }
        .buttonStyle(PlainButtonStyle())
    }
}

struct RoundedButton_Previews: PreviewProvider {
    static var previews: some View {
        VStack(spacing: 16) {
            RoundedButton(title: "This is a button", leadingView: {
                Image(systemName: "pencil.and.outline")
                    .foregroundColor(.white)
            }, action: {
                print("Tap")
            })
            
            RoundedButton(
                title: "This is a loading button",
                isLoading: true,
                leadingView: {
                    Image(systemName: "scribble.variable")
                        .foregroundColor(.white)
                }, action: {
                    print("Tap")
                })
        }
    }
}

extension RoundedButton where LeadingView == EmptyView {
    
    init(
        title: String,
        isLoading: Bool = false,
        action: @escaping () -> Void = {}
    ) {
        self.init(
            title: title,
            isLoading: isLoading,
            leadingView: { EmptyView() },
            action: action
        )
    }
    
}
