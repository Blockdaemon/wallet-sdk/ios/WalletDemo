// Copyright © Blockdaemon All rights reserved.

import SwiftUI

@main
struct WalletDemoApp: App {
    var body: some Scene {
        WindowGroup {
            RootView()
        }
    }
}
